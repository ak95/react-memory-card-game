# react-memory-card-game

Memory card game implemented in React

Rules are simple: click on unique cards. If the card is not unique, score is reset. Best score is saved.