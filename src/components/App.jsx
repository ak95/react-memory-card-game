import Cards from "./Cards.jsx";
import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

export default function App() {
  const [API, setAPI] = useState(null);
  useEffect(() => {
    // saves API
    fetch("https://pokeapi.co/api/v2/generation/1/")
      .then((response) => response.json())
      .then((value) => setAPI(value));
  }, []);
  return API ? (
    <>
      <header className="d-flex flex-wrap justify-content-between align-items-center py-2 px-5 border-bottom">
        <div className="d-flex flex-column align-items-start gap-2">
          <h1 className="fs-3 float-md-start mb-0">Pokémon Memory Game</h1>
          <h2 className="fs-6 mb-0 fst-italic">
            Click on Pokémon to increase your score, but don't click any Pokémon
            twice!
          </h2>
        </div>
        <div className="d-flex">
          <ul className="list-group list-group-flush mb-0 w-100">
            <li className="list-group-item d-flex justify-content-between align-items-center">
              Current Score:
              <span
                className="badge bg-light text-dark rounded-pill"
                id="score"
              ></span>
            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
              Best Score:
              <span
                className="badge bg-light text-dark rounded-pill"
                id="best"
              ></span>
            </li>
          </ul>
        </div>
      </header>
      <Cards api={API} />
    </>
  ) : (
    <div className="h-100 w-100 d-flex justify-content-center align-items-center">
      <FontAwesomeIcon icon={faSpinner} size="2xl" spinPulse />
    </div>
  );
}
