import { useState, useEffect, useRef, useCallback } from "react";
import { v4 as uuidv4 } from "uuid";

export default function Cards({ api }) {
  const [pokemon, setPokemon] = useState([]);
  const [score, setScore] = useState(0);
  const [bestScore, setBestScore] = useState(0);

  function handleClickCB(index, clickBool) {
    let temp = [...pokemon]; // get pokemon array

    if (clickBool === true) {
      if (score > bestScore) {
        setBestScore(score);
      }
      setScore(0);
      // unclick ALL the things!
      for (let i = 0; i < temp.length; i++) {
        const oldCard = temp[i]; // get card at index
        if (oldCard.clicked === true) {
          const newCard = {
            number: oldCard.number,
            clicked: false,
          }; // make new card and change in array
          temp[i] = newCard;
        }
      }
    } else {
      // if unclicked
      if (score === 11) {
        // at score limit
        alert("Congratulations! You got all the cards!");
        window.location.reload(false);
      }
      // otherwise
      setScore(score + 1); // append to score

      // set clicked state
      const mapped = temp.map(({ number, clicked }) => number); // extract just the pokedex number
      const currentIndex = mapped.indexOf(
        mapped.find((number) => number === index)
      ); // find the index of the current pokedex number in number-only array
      let current = temp[currentIndex]; // since it's a mapped copy of the original, same index applies
      current.clicked = true; // now set the clicked prop of the current card in index
      temp[currentIndex] = current; // plug back into temp array
    }

    // rng new values into output array
    const output = [];
    while (temp.length) {
      const index = Math.floor(Math.random() * temp.length); // get random index
      output.push(temp.splice(index, 1)[0]); // pushes the random index value to array while removing it from temp array
    }
    setPokemon(output);
  }

  const Card = ({ pokedexNumber, clicked, callback }) => {
    const [cardClass, setCardClass] = useState(
      "d-flex flex-column bg-light text-dark card rounded-3 justify-content-around"
    );
    function handleChange(event) {
      if (event.type === "mousedown") {
        setCardClass(
          "d-flex flex-column bg-light text-dark card rounded-3 justify-content-around border border-primary border-5"
        );
      } else if (event.type === "mouseup") {
        setCardClass(
          "d-flex flex-column bg-light text-dark card rounded-3 justify-content-around"
        );
        callback(pokedexNumber, clicked);
      }
    }
    const temp = api.pokemon_species[pokedexNumber].name;
    const species = temp.replace(temp.charAt(0), temp.charAt(0).toUpperCase());
    const imgNumber = api.pokemon_species[pokedexNumber].url
      .replace(
        new RegExp("https://pokeapi.co/api/v2/pokemon-species/", "gi"),
        ""
      )
      .replace("/", "");
    const imgSrc = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${imgNumber}.png`;
    return (
      <button
        className={cardClass}
        onMouseDown={handleChange}
        onMouseUp={handleChange}
      >
        <img src={imgSrc} className="card-img-top" alt={species}></img>
        <h3 className="card-title fs-5">
          #{pokedexNumber} {species}
        </h3>
      </button>
    );
  };

  // runs on first load
  useEffect(() => {
    // immediately loads user scores into DOM
    document.getElementById("score").textContent = score;
    document.getElementById("best").textContent = bestScore;
    // makes cards
    let indexes = [];
    let batched = [];
    for (let i = 0; i < 12; i++) {
      let index;
      function roll() {
        index = parseInt(Math.random() * 151); // get random number between 0 and 151
      }
      roll(); // immediately roll to initialize index
      if (indexes.length > 0) {
        for (let z = 0; z < indexes.length; z++) {
          while (indexes[z] === index) {
            roll();
          }
        }
      }
      // if made it to here, roll is valid
      indexes.push(index);
      batched.push({
        number: index,
        clicked: false,
      });
    }
    setPokemon(batched);
  }, []);

  // runs on change in score or bestScore
  useEffect(() => {
    // loads user scores into DOM
    document.getElementById("score").textContent = score;
    document.getElementById("best").textContent = bestScore;
  }, [score, bestScore]);

  return (
    <div className="d-flex flex-column p-4 flex-1-0-auto">
      <div
        className="d-flex flex-row flex-wrap gap-5 justify-content-around"
        id="cardholder"
      >
        {pokemon.map((object) => {
          const index = object.number;
          const wasClicked = object.clicked;
          return (
            <Card
              pokedexNumber={index}
              key={uuidv4()}
              clicked={wasClicked}
              callback={handleClickCB}
            />
          );
        })}
      </div>
    </div>
  );
}
